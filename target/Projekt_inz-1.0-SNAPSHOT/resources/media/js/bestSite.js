var bestSiteData;
$.ajax({
    type: "GET",
    url: "bestSite"
}).done(function(msg) {
    bestSiteData=JSON.parse(msg);
    bestSiteGrid()
});
function bestSiteGrid() {
    require(['dojo/_base/lang', 'dojox/grid/DataGrid', 'dojo/data/ItemFileWriteStore', 'dojo/dom', 'dojo/domReady!'],
            function(lang, DataGrid, ItemFileWriteStore, dom) {

                /*set up data store*/
                var data = {
                    identifier: "lp",
                    items: []
                };
                var data_list = bestSiteData;
                var rows = 500;
                for (var i = 0, l = data_list.length; i < rows; i++) {
                    data.items.push(data_list[i]);
                }
                var store = new ItemFileWriteStore({data: data});

                /*set up layout*/
                var layout = [[
                        {'name': 'lp', 'field': 'lp', 'width': '10%'},
                        {'name': 'adres', 'field': 'adres', 'width': '60%'},
                        {'name': 'wejść', 'field': 'wejsc', 'width': '15%'},
                        {'name': '% wejść', 'field': 'wejscpro', 'width': '15%'}
                    ]];

                /*create a new grid*/
                var grid = new DataGrid({
                    id: 'grid',
                    store: store,
                    structure: layout,
                    rowSelector: '20px'});

                /*append the new grid to the div*/
                grid.placeAt("bestSite");

                /*Call startup() to render the grid*/
                grid.startup();
            });
}