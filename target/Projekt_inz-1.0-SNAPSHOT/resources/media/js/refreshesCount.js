



require(["dojox/charting/Chart",
    "dojox/charting/axis2d/Default",
    "dojox/charting/plot2d/Markers",
    "dojox/charting/themes/Claro",
    "dojox/charting/action2d/Tooltip",
    "dojo/ready"],
        function(Chart, Default, Markers, Claro, Tooltip, ready) {
            ready(function() {
                var c = new Chart("refreshesCount");
                c.addPlot("default", {type: Markers, tension: 0})
                        .addAxis("x", {fixLower: "major", fixUpper: "major"})
                        .addAxis("y", {vertical: true, fixLower: "major", fixUpper: "major", min: 0})
                        .setTheme(Claro);
                $.ajax({
                    type: "GET",
                    url: "logsPerDay"
                }).done(function(msg) {

                    data = JSON.parse(msg);
                    c.addSeries("logsPerDay", data);
                    new dojox.charting.action2d.Tooltip(c, "default");
                    c.render();
                });


            });
        });


