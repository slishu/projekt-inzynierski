/*---------LEKTOR------------------------------------------------------*/

function lektorStartStop(file) {
//file = "/media/music/puppet_speaker/puppet0"

    my_media_was_playing = my_media_playing;

    if (my_lektor == null) {
        var new_file = file.replace("/media/music/puppet_speaker", "/android_asset/www/sound");
        my_lektor = new Media(new_file + ".mp3", onSuccess, onError, resetLektor);
        if (my_media_was_playing) {
            my_media.pause();
        }
        my_lektor.play();
        playing = true;
    } else {
        if (playing) {
            if (my_media_was_playing) {
                my_media.play();
            }
            my_lektor.stop();
            my_lektor.null;
        } else {
            if (my_media_was_playing) {
                my_media.pause();
            }
            my_lektor.play();
        }
    }

}

var resetLektor = function(status) {
    if (status === Media.MEDIA_STOPPED) {
        if (my_lektor != null)
            if (my_media_was_playing) {
                my_media.play();
            }
        my_lektor = null;
    }
};
var my_media_was_playing = false;
var my_lektor = null;
var playing = false;

//volumeStartStop


console.log("loading startStop script")

$(document).ready(function() {

    searchForStartStop();

});


/*---------WYCISZANIE------------------------------------------------------*/
var tried = 0;
function searchForStartStop() {

    var found = false

    for (i = -1; i < 4; i++) {
        var element = null;
        var name = "";
        if (i == -1) {
            name = "#Stage_music_x_music_x";
        } else {
            name = '#Stage_music_x' + i + '_music_x';
        }
        element = $(name);

        console.log("testing " + tried, name, element, element.get(0))
        if (element.get(0)) {
            found = true;
            console.log("added changeStop")

            element.on("click", changeStop);
            console.log("name for anty ", name.substr(0, name.length - 2))
            $(name.substr(0, name.length - 2)).on("click", changeStart);
            changeStop();
            changeStart();
        }

    }
    console.log(found, tried, "Xxxxxxxxxxxxxxxxxxxxx");
    if (found == false && tried < 40) {

        tried++;
        setTimeout("searchForStartStop()", 500);
    } else {
        console.log("found: " + name);

    }


}


function changeStop() {

    my_media.pause();


}
function changeStart() {
    my_media.play();


}
console.log("loaded startStop script")


/*---------MUZYKA------------------------------------------------------*/


var my_media = null;
var mediaTimer = null;
var my_media_playing = false;

function playAudio(src) {
    my_media_playing = true;
    if (my_media == null) {
        // Create Media object from src
        my_media = new Media(src, onSuccess, onError, loop);
    } // else play current audio
    // Play audio
    my_media.play();

    // Update my_media position every second
    if (mediaTimer == null) {
        mediaTimer = setInterval(function() {
            // get my_media position
            my_media.getCurrentPosition(
                    // success callback
                            function(position) {
                                if (position > -1) {
                                    setAudioPosition((position) + " sec");
                                }


                            },
                            // error callback
                                    function(e) {
                                        console.log("Error getting pos=" + e);
                                        setAudioPosition("Error: " + e);
                                    }
                            );
                        }, 1000);
            }
}

// Pause audio
//
function pauseAudio() {
    if (my_media) {
        my_media_playing = false;
        my_media.pause();
    }
}

// Stop audio
//
function stopAudio() {
    if (my_media) {
        my_media_playing = false;
        my_media.stop();
    }
    clearInterval(mediaTimer);
    mediaTimer = null;
}

// onSuccess Callback
//
function onSuccess() {
    console.log("playAudio():Audio Success");
}

// onError Callback
//
function onError(error) {
    // alert('code: '    + error.code    + '\n' +
    //        'message: ' + error.message + '\n');
}
var loop = function(status) {
    if (status === Media.MEDIA_STOPPED) {
        my_media.play();
        my_media_playing = true;
    }
};


