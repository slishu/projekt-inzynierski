require([
                'dojo/ready',
                'dojo/request',
                'dojo/json',
                'dojo/_base/array',
                'dojo/store/Observable',
                'dojo/store/Memory',
                'dojox/charting/Chart2D',
                'dojox/charting/themes/Claro',
                'dojox/charting/StoreSeries'
            ], function(ready, request, json, array, Observable, Memory, Chart2D, Claro, StoreSeries) {

                ready(function() {

                    // store
                    var store = new Observable(new Memory({
                        data: {
                            identifier: 'id',
                            label: 'Users Online',
                            items: []
                        }
                    }));
                    // chart
                    chart = new Chart2D('activeUsers');
                    chart.setTheme(Claro);
                    chart.addPlot('default', {type: 'Columns', markers: true});
                    chart.addAxis('x', {min: 0, max: 10, fixUpper: 'major'});
                    chart.addAxis('y', {vertical: true, fixLower: 'major', fixUpper: 'major', minorTickStep: 1});
                    chart.addSeries('series1', new StoreSeries(store, {query: {}}, 'value'));

                    chart.render();
                    // data update
                    setInterval(function() {
                        request('/Projekt_inz/activeUsers').then(function(response) {
                            response = json.parse(response, true);
                            array.forEach(response.items, function(item) {
                                if (item.id > 20)
                                    store.remove(item.id - 20 )
                                store.notify(item);

                            });
                        });
                    }, 1000);
                });
            });