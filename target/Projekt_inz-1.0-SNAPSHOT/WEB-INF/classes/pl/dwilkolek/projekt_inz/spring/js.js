function connectorTurbit() {
    this.uid = null;
    this.sid = null;
    this.lid = "";
    this.cid = "{#cid#}";
    this.host = "projekt.turbit.pl";
    this.ip = "0";
    this.czasSesji = "{#czasSesji#}";
    this.header = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ws="http://ws.projekt_inz.dwilkolek.pl/">' +
            '<soap:Header/>' +
            '<soap:Body>';
    this.footer = '</soap:Body>' +
            '</soap:Envelope>'
    this.logCounter = 0;
    this.instance = this;


    var OSName = "Unknown OS";
    if (navigator.appVersion.indexOf("Win") != -1)
        OSName = "Windows";
    if (navigator.appVersion.indexOf("Mac") != -1)
        OSName = "MacOS";
    if (navigator.appVersion.indexOf("X11") != -1)
        OSName = "UNIX";
    if (navigator.appVersion.indexOf("Linux") != -1)
        OSName = "Linux";

    this.os = OSName;
    this.resolution = screen.width + 'x' + screen.height;


    this.setCookie = function(name, value, seconds) {
        console.log("setting up cookie " + name + "=" + value + " for " + seconds)
        var date, expires;
        if (seconds) {
            date = new Date();
            date.setTime(date.getTime() + (seconds * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    this.getCookie = function(c_name) {


        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1)
        {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1)
        {
            c_value = null;
        }
        else
        {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1)
            {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }

        return c_value;
    }
    this.getIp = function(callback) {
        var _this = this;
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {

                _this.ip = xmlhttp.responseText;
                return _this.ip;
            }
        }
        xmlhttp.open("GET", "http://" + this.host + "/ip", true);
        xmlhttp.send();

    }

    this.logByJs = function() {

        var _this = this;
        (function(instance) {

            var js = document.createElement('script');
            js.type = 'text/javascript';
            js.async = true;
            var d = new Date().getTime();
            js.src = 'http://' + 'projekt.turbit.pl/logByJs?' +
                    'cid=' + instance.cid +
                    '&uid=' + instance.uid +
                    '&sid=' + instance.sid +
                    '&lid=' + '' +
                    '&browser=' + navigator.userAgent +
                    '&os=' + instance.os +
                    '&resolution=' + instance.resolution +
                    '&href=' + window.location +
                    '&refferal=' + document.referrer +
                    '&nochache=' + d;
            console.log(js);
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(js, s);
        })(_this);
    }





    this.fLog = function() {
        var _this = this;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', 'http://' + this.host + '/Service', true);
        var sr = '<ws:fLog>' +
                '<uid>' + this.uid + '</uid>' +
                '<browser>' + navigator.userAgent + '</browser>' +
                '<cid>' + this.cid + '</cid>' +
                '<os>' + this.os + '</os>' +
                '<resolution>' + this.resolution + '</resolution>' +
                '<sid>' + _this.sid + '</sid>' +
                '<href>' + window.location + '</href>' +
                '<refferal>' + document.referrer + '</refferal>' +
                '<lid>' + this.lid + '</lid>' +
                '</ws:fLog>'

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    returnMsg = xmlhttp.responseXML.getElementsByTagName("return")[0].textContent;
                    returnMsg = returnMsg.split("#");
                    _this.lid = returnMsg[2];
                    _this.sid = returnMsg[1];
                    _this.uid = returnMsg[0];
                    _this.setCookie("sid", _this.sid, _this.czasSesji * 60);
                    _this.log();
                }
            }
        }
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send(_this.header + sr + _this.footer);





    }

    this.saveLogByJs = function() {
        this.cid = window.turbit_cid;
        this.sid = window.turbit_sid;
        this.lid = window.turbit_lid;
        this.setCookie("sid", this.sid, this.czasSesji * 60);
        this.setCookie("uid", this.uid, 5184000);
        this.logLidByJs();
    }
    this.logLidByJs = function() {
        var _this = this;

//        if (this.tabActive) {
            (function(instance) {

                this.setCookie("sid", this.sid, this.czasSesji * 60);
                this.setCookie("uid", this.uid, 5184000);
                ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                d = new Date().getTime();
                ga.src = 'http://' + 'projekt.turbit.pl/logLid?lid=' + instance.lid + '&nocache=' + d;
                window.turbitScript = ga;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(window.turbitScript, s);
                setTimeout(function() {

                   //  window.turbitScript.parentNode.removeChild(window.turbitScript);
                }, 500);

            })(_this);
//        }

    }

    this.log = function() {

        var _this = this;
//        if (window.connectorTurbitInstanceTabActive !== undefined) {
//            this.tabActive = window.connectorTurbitInstanceTabActive;
//        }
//        console.log("tab is active?", this.tabActive, _this.logCounter, _this.czasSesji * 30);
//        if (this.tabActive) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open('POST', 'http://' + this.host + '/Service', true);
            var sr = '<ws:log>' +
                    '<lid>' + this.lid + '</lid>' +
                    '</ws:log>';

            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.status == 200) {
                        _this.logCounter++;
                        if (_this.logCounter > _this.czasSesji * 30) {
                            _this.logCounter = 0;
                            _this.setCookie("sid", _this.sid, _this.czasSesji * 60);
                        }
                        _this.lid = xmlhttp.responseXML.getElementsByTagName("return")[0].textContent;
                        setTimeout(function() {
                            _this.log();
                        }, 1000);
                    }
                }
            }

            xmlhttp.setRequestHeader('Content-Type', 'text/xml');
            xmlhttp.send(this.header + sr + this.footer);
//        } else {
//            setTimeout(function() {
//                _this.log();
//            }, 1000);
//        }
    }
    this.createLog = function() {
        var _this = this;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', 'http://' + this.host + '/Service', true);
        var sr = '      <ws:createLog>' +
                '<sid>' + _this.sid + '</sid>' +
                '<href>' + window.location + '</href>' +
                '<refferal>' + document.referrer + '</refferal>' +
                '</ws:createLog>';

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    _this.lid = xmlhttp.responseXML.getElementsByTagName("return")[0].textContent;
                    _this.setCookie("sid", _this.sid, _this.czasSesji * 60);
                    _this.log();
                }
            }
        }
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send(_this.header + sr + _this.footer);
    }
    this.createSession = function() {
        var _this = this;

        if (this.ip.length < 6) {
            this.getIp()
        }

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', 'http://' + this.host + '/Service', true);
        var sr = '<ws:createSession>' +
                '<uid>' + this.uid + '</uid>' +
                '<browser>' + navigator.userAgent + '</browser>' +
                '<cid>' + this.cid + '</cid>' +
                '<os>' + this.os + '</os>' +
                '<resolution>' + this.resolution + '</resolution>' +
                '</ws:createSession>';

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    _this.sid = xmlhttp.responseXML.getElementsByTagName("return")[0].textContent;
                    console.log(xmlhttp.responseXML.getElementsByTagName("return"))
                    _this.setCookie("sid", _this.sid, _this.czasSesji * 60);
                    _this.createLog();
                }
            }
        }
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send(_this.header + sr + _this.footer);
    }
    this.createUser = function() {
        var _this = this;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', 'http://' + this.host + '/Service', true);
        var sr = '<ws:createUser/>';

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    console.log(xmlhttp.responseXML.getElementsByTagName("return"), xmlhttp.responseXML)
                    _this.uid = xmlhttp.responseXML.getElementsByTagName("return")[0].textContent;
                    console.log(xmlhttp.responseXML.getElementsByTagName("return"))
                    _this.setCookie("uid", _this.uid, 5184000);
                    _this.createSession();
                }
            }
        }
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send(_this.header + sr + _this.footer);
    }


    this.tabActive = true;
//    this.clear = function() {
//        window.connectorTurbitInstanceTabActive = false;
//        if (/*@cc_on!@*/false) {
//            document.onfocusin = connectorTurbitInstance.set
//        } else {
//            window.onfocus = connectorTurbitInstance.set
//        }
//
//    }
//    this.set = function() {
//        window.connectorTurbitInstanceTabActive = true;
//        if (/*@cc_on!@*/false) {
//            document.onfocusin = null;
//        } else {
//            window.onfocus = null;
//        }
//    }



    if (!(this.getCookie("uid") === null || this.getCookie("uid") === undefined)) {
        this.uid = this.getCookie("uid");
        if (this.uid == "undefined")
            this.uid = "";
    } else {
        this.uid = "";
    }

    if (!(this.getCookie("sid") === null || this.getCookie("sid") === undefined)) {
        this.sid = this.getCookie("sid");
        if (this.sid == "undefined")
            this.sid = "";
    } else {
        this.sid = "";
    }

    this.logByJs();
}
function turbitCallLog() {
    window.connectorTurbitInstance.logLidByJs();
    setTimeout("turbitCallLog()", 1300);
}
window.turbitScript;

window.connectorTurbitInstanceTabActive = false;
window.connectorTurbitInstance = new connectorTurbit();

//if (/*@cc_on!@*/false) { // check for Internet Explorer
//    document.onfocusin = connectorTurbitInstance.set
//    document.onfocusout = connectorTurbitInstance.clear
//} else {
//    window.onfocus = connectorTurbitInstance.set
//    window.onblur = connectorTurbitInstance.clear
//
//}
connectorTurbitInstance.set();





	