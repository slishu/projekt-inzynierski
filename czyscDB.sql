﻿Truncate table log RESTART IDENTITY CASCADE;
Truncate table session RESTART IDENTITY CASCADE;
Truncate table "user" RESTART IDENTITY CASCADE;
Truncate table stats RESTART IDENTITY CASCADE;
SELECT (SELECT COUNT(*) as a FROM log),
	(SELECT COUNT(*) as b FROM session),
	(SELECT COUNT(*) as c FROM "user"),
	(SELECT COUNT(*) as d FROM stats);
