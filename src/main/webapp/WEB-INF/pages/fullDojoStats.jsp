<%-- 
    Document   : fullDojoStats
    Created on : 2013-11-28, 05:00:47
    Author     : Slishu
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Language" content="pl" /> 
        <style type="text/css">
            @import
            url("http://archive.dojotoolkit.org/nightly/dojotoolkit/dijit/themes/claro/claro.css");
        </style>
        <link href="http://ajax.googleapis.com/ajax/libs/dojo/1.9.1/dojox/grid/resources/Grid.css" rel="stylesheet" type="text/css"/>
        <link href="http://ajax.googleapis.com/ajax/libs/dojo/1.9.1/dojox/grid/resources/claroGrid.css" rel="stylesheet" type="text/css"/>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
        <link type="text/css" href="<c:url value="/media/css/main.css" />"  rel='stylesheet' />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/dojo/1.9.1/dojo/dojo.js"></script>
        <title>JSP Page</title>
        <style>
            td{
                width:33%;
            }
        </style>
    </head>
    <body class="claro">
        <h1>Dane statystyczne</h1>
        <table style="width:100%;">
            <tr>
                <td colspan="2">
                    <h2>Aktualnie na stronie: <span id="activeUsersNumber"></span> użytkowników</h2>
                    <div id="activeUsers" style="width: 100%; height: 100%;"></div>
                    <script src="<c:url value="/media/js/activeUsers.js" />"></script>
                </td>

                <td>
                    <h2>Nowe wejścia <span id="returningVisitorsNumber"></span></h2>
                    <div id="returningVisitors" style="width: 100%; height: 100%;"></div>
                    <script src="<c:url value="/media/js/returningVisitors.js" />"></script> 
                </td>
            </tr>
            <tr>
                <td class="textStats">
                    <h4> Zapisanych użytkowników:  <span id="savedUsers"></span></h4>
                    <h4> Zapisanych sesji:   <span id="savedSessions"></span></h4>
                    <h4> Zapisanych odwiedzin:  <span id="savedLogs"></span></h4>
                    <h4> Zapisanych logów:   <span id="savedStats"></span></h4>


                </td class="textStats">
                <td>
                    <h4> Aktywnych sesji:   <span id="activeSessions"></span></h4>
                    <h4> Zapytań do web serwisu: <span id="msgRate"></span></h4>
                    <h4> Średni czas sesji: <span id="avgTimeOnSite"></span></h4>
                    <h4> Średnia ilość podstron/sesję: <span id="avgSitesViewed"></span></h4>
                    <script src="<c:url value="/media/js/textStats.js" />"></script>
                </td>
                <td>
                    <h2>Współczynnik odrzuceń <span id="refuseProportionNumber"></span></h2>
                    <div id="refuseProportion" style="width: 100%; height: 100%;"></div>
                    <script src="<c:url value="/media/js/refuseProportion.js" />"></script> 
                </td>

            </tr>
            <tr>
                <td>
                    <h2>Ranking przeglądarek <span id="bestBrowserNumber"></span></h2>
                    <div id="bestBrowser" style="width: 100%; height: 100%;"></div>
                    <script src="<c:url value="/media/js/bestBrowser.js" />"></script>
                </td>
                <td>
                    <h2>Ranking rozdzielczości <span id="bestResolutionNumber"></span></h2>
                    <div id="bestResolution" style="width: 100%; height: 100%;"></div>
                    <script src="<c:url value="/media/js/bestResolution.js" />"></script>                    
                </td>
                <td>
                    <h2>Ranking OS <span id="bestOsNumber"></span></h2>
                    <div id="bestOs" style="width: 100%; height: 100%;"></div>
                    <script src="<c:url value="/media/js/bestOs.js" />"></script> 
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <h2>Wizyt i unikalnych wizyt dziennie</h2>
                    <div id="uniqueUsersPerDay" style="width: 100%; height: 20%;"></div>
                    <script src="<c:url value="/media/js/UsersPerDay.js" />"></script>
                </td>
            </tr>
            <tr>
                <td colspan="3">

                    <h2>Najpopularniejsze strony <span id="returningVisitorsNumber"></span></h2>
                    <div id="bestSite" style="width: 100%;height: 600px;"></div>
                    <script src="<c:url value="/media/js/bestSite.js" />"></script> 

                </td>
            </tr>
        </table>














    </body>
</html>
