<%-- 
    Document   : db-stats
    Created on : 2013-11-24, 03:46:25
    Author     : Slishu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Stats</title>
    </head>
    <body onload="reload()">
        <h1>Stats</h1>
        zapisanych użytkowników: ${dataUserCount} / uzytkowników na stronie ${dataActiveUsers}<br />
        zapisanych sesji: ${dataSessionCount} / sesji aktywnych ${dataSessionActiveCount} <br />
        zapisanych logów: ${dataLogCount} <br />
        rekordów stats: ${dataStatsCount} <br />  
        Web serwis: ${msgRate} zapytań/min <br />

        <script type="text/javascript">
            function reload() {
                setTimeout("location.reload(true);", 1000);
            }
        </script>
    </body>
</html>
