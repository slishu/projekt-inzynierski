function refreshTextStats() {
    $.ajax({
        type: "GET",
        url: "msgRate"
    }).done(function(msg) {
        $("#msgRate").html(msg)
    });
    $.ajax({
        type: "GET",
        url: "activeSessions"
    }).done(function(msg) {
        $("#activeSessions").html(msg)
    });
    $.ajax({
        type: "GET",
        url: "statsCount"
    }).done(function(msg) {
        $("#savedStats").html(msg)
    });
    $.ajax({
        type: "GET",
        url: "logCount"
    }).done(function(msg) {
        $("#savedLogs").html(msg)
    });
    $.ajax({
        type: "GET",
        url: "sesionCount"
    }).done(function(msg) {
        $("#savedSessions").html(msg)
    });
    $.ajax({
        type: "GET",
        url: "userCount"
    }).done(function(msg) {
        $("#savedUsers").html(msg)
    });
    $.ajax({
        type: "GET",
        url: "avgTimeOnSite"
    }).done(function(msg) {
        $("#avgTimeOnSite").html(msg)
    });
        $.ajax({
        type: "GET",
        url: "avgSitesViewed"
    }).done(function(msg) {
        $("#avgSitesViewed").html(msg)
    });
    setTimeout(function() {
        refreshTextStats();
    }, 60000)
}
refreshTextStats();