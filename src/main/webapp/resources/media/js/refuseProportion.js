// When the DOM is ready and resources are loaded...

require([
    // Require the basic 2d chart resource
    "dojox/charting/Chart",
    // Require the theme of our choosing
    "dojox/charting/themes/Claro",
    // Charting plugins: 
    // 	Require the Pie type of Plot 
    "dojox/charting/action2d/Tooltip",
    "dojox/charting/plot2d/Pie",
    // Wait until the DOM is ready

    "dojo/domReady!"
], function(Chart, Claro, Tooltip, PiePlot) {

    // Create the chart within it's "holding" node
    var pieChart = new Chart("refuseProportion");

    // Set the theme
    pieChart.setTheme(Claro);

    // Add the only/default plot 
    pieChart.addPlot("default", {
        type: PiePlot, // our plot2d/Pie module reference as type value
        radius: 200,
        label: true,
        fontColor: "black",
        labelOffset: -20
    });

    $.ajax({
        type: "GET",
        url: "refuseProportion"
    }).done(function(msg) {
        refuseProportion = JSON.parse(msg)
        pieChart.addSeries("Systemy operacyjne", refuseProportion);
        new dojox.charting.action2d.Tooltip(pieChart, "default");
        pieChart.render();
    });



});

