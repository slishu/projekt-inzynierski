var userPerDay1;
var userPerDay2;
var userPerDay3;
$.ajax({
    type: "GET",
    url: "usersPerDay"
}).done(function(msg) {
    userPerDay2 = JSON.parse(msg);
    getUserPerDay2Data();
});

function getUserPerDay2Data() {
    $.ajax({
        type: "GET",
        url: "uniqueUsersPerDay"
    }).done(function(msg) {
        userPerDay1 = JSON.parse(msg)
        getLogsPerDay3Data()
    });
}
function getLogsPerDay3Data() {
    $.ajax({
        type: "GET",
        url: "logsPerDay"
    }).done(function(msg) {

        userPerDay3 = JSON.parse(msg);
        loadChartUsersPerDay()

    });
}
function loadChartUsersPerDay() {
    require(["dojox/charting/Chart",
        "dojox/charting/axis2d/Default",
        "dojox/charting/plot2d/Markers",
        "dojox/charting/themes/Claro",
        "dojox/charting/action2d/Tooltip",
        "dojo/ready"],
            function(Chart, Default, Markers, Claro, Tooltip, ready) {
                ready(function() {
                    var c = new Chart("uniqueUsersPerDay");
                    c.addPlot("default", {type: Markers, tension: 0})
                            .addAxis("x", {fixLower: "major", fixUpper: "major", labelFunc: labelfTime})
                            .addAxis("y", {vertical: true, fixLower: "major", fixUpper: "major", min: 0})
                            .setTheme(Claro);
                    c.addSeries("uniqueUsersPerDay", userPerDay1);
                    //   c.addSeries("UsersPerDay", userPerDay2);
                    c.addSeries("LogsPerDay", userPerDay3);
                    function labelfTime(o)
                    {
                        var dt = new Date();
                       console.log(o);
                       dt.setDate(dt.getDate()-(30-o));
                   
                                return dt.getDate()+"/"+(dt.getMonth()+1)+"/"+dt.getFullYear();
                    }
                    new dojox.charting.action2d.Tooltip(c, "default");
                    c.render();

                });
            });


}