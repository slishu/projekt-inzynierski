<%-- 
    Document   : fullDojoStats
    Created on : 2013-11-28, 05:00:47
    Author     : Slishu
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/dojo/1.9.1/dojo/dojo.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dojo workout</h1>

        <h2>Aktualnie na stronie: <span id="activeUsersNumber"></span> użytkowników</h2>
        <div id="activeUsers" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/activeUsers.js" />"></script>

        <h2>Unikalne odwiedziny</h2>
        <div id="uniqueUsersPerDay" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/uniqueUsersPerDay.js" />"></script>

        <h2>Powracający użytkownicy <span id="returningVisitorsNumber"></span></h2>
        <div id="returningVisitors" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/returningVisitors.js" />"></script>     

        <h2>Odsłony <span id="refreshesCountNumber"></span></h2>
        <div id="refreshesCount" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/refreshesCount.js" />"></script>     

        <h2>Współczynnik odrzuceń <span id="refuseProportionNumber"></span></h2>
        <div id="refuseProportion" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/refuseProportion.js" />"></script>    

        <h2>Stron/wizyta <span id="pagesPerSessionNumber"></span></h2>
        <div id="pagesPerSession" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/pagesPerSession.js" />"></script>     

        <h2>Średni czas trwania sesji <span id="avarageSessionTimeNumber"></span></h2>
        <div id="avarageSessionTime" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/avarageSessionTime.js" />"></script>     

        <h2>Średni czas przeglądania pojedyńczej stronie <span id="avarageLogTimeNumber"></span></h2>
        <div id="avarageLogTime" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/avarageLogTime.js" />"></script>    

        <h2>Ranking przeglądarek <span id="bestBrowserNumber"></span></h2>
        <div id="bestBrowser" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/bestBrowser.js" />"></script>  

 

        <h2>Dziennie wizyt <span id="dailySessionsNumber"></span></h2>
        <div id="dailySessions" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/dailySessions.js" />"></script>  

        <h2>Dziennie odsłon <span id="dailyLogsNumber"></span></h2>
        <div id="dailyLogs" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/dailyLogs.js" />"></script>  

        <h2>Dziennie użytkowników <span id="dailyUsersNumber"></span></h2>
        <div id="dailyUsers" style="width: 20%; height: 20%;"></div>
        <script src="<c:url value="/media/js/dailyUsers.js" />"></script>  

        <h2>Ranking stron <span id="bestSiteNumber"></span></h2>
        <div id="bestSite" style=""></div>
        <script src="<c:url value="/media/js/bestSite.js" />"></script>  
        
    </body>
</html>
