var activeUsers = 0;
require([
    'dojo/ready',
    'dojo/request',
    'dojo/json',
    'dojo/_base/array',
    'dojo/store/Observable',
    'dojo/store/Memory',
    'dojox/charting/Chart2D',
    'dojox/charting/themes/Claro',
    'dojox/charting/StoreSeries'
], function(ready, request, json, array, Observable, Memory, Chart2D, Claro, StoreSeries) {

    ready(function() {

        // store
        var store = new Observable(new Memory({
            data: {
                identifier: 'id',
                label: 'Users Online',
                items: []
            }
        }));
        // chart
        chart = new Chart2D('activeUsers');
        chart.setTheme(Claro);
        chart.addPlot('default', {type: 'Columns', markers: true});
        chart.addAxis('x', {min: 0, max: 43});
        chart.addAxis('y', {min: 0, vertical: true, fixUpper: 'major', minorTickStep: 1});
        chart.addSeries('series1', new StoreSeries(store, {query: {}}, 'value'));

        request('activeUsers').then(function(response) {
            response = json.parse(response, true);
            array.forEach(response.items, function(item) {
                item.id = ItemIdActiveUserNumber;
                ItemIdActiveUserNumber++;
                store.notify(item);

                if (item.id > 40)
                    store.remove(item.id - 40)
                activeUsers = item.value;
                $("#activeUsersNumber").html(activeUsers);
            });
        });
        chart.render();

        setInterval(function() {
            request('activeUsers').then(function(response) {
                response = json.parse(response, true);
                array.forEach(response.items, function(item) {
                    item.id = ItemIdActiveUserNumber;
                    ItemIdActiveUserNumber++;
                    store.notify(item);

                    if (item.id > 40)
                        store.remove(item.id - 40)
                    activeUsers = item.value;
                    $("#activeUsersNumber").html(activeUsers);
                });
            });
        }, 10000);
    });
});

var ItemIdActiveUserNumber = 0;