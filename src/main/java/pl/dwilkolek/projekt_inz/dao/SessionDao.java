/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.dao;

import pl.dwilkolek.projekt_inz.model.Session;

/**
 *
 * @author Slishu
 */
public interface SessionDao {
    Session add(Session session);
    Session update(Session session);
    Session findBySid(long sid);
}
