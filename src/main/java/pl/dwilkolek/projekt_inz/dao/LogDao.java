/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.dao;

import pl.dwilkolek.projekt_inz.model.Log;

/**
 *
 * @author Slishu
 */
public interface LogDao {
    Log add(Log log);
    Log update(Log log);
    
    Log findByLid(long lid);
    
}
