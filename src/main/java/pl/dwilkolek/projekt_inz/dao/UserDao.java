/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.dao;

import pl.dwilkolek.projekt_inz.model.User;

/**
 *
 * @author Slishu
 */
public interface UserDao {
    User add(User user);
    
    User findByUid(long uid);
}
