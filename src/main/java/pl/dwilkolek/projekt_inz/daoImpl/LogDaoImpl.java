/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.daoImpl;

import org.hibernate.Session;
import pl.dwilkolek.projekt_inz.dao.LogDao;
import pl.dwilkolek.projekt_inz.model.Log;
import pl.dwilkolek.projekt_inz.hibernate.HibernateUtility;


/**
 *
 * @author Slishu
 */
public class LogDaoImpl implements LogDao {

    private Session session;

    public LogDaoImpl() {

        this.session = HibernateUtility.getSessionFactory().openSession();
        
    }

    @Override
    public Log add(Log log) {
        session.beginTransaction();
        session.save(log);
        session.getTransaction().commit();
        

        return log;
    }

    @Override
    public Log update(Log log) {
        session.beginTransaction();
        session.update(log);       
        session.getTransaction().commit();
        return log;
    }

    @Override
    public Log findByLid(long lid) {
         session.beginTransaction();
         Log log = (Log) session.get(Log.class, lid);
         session.getTransaction().commit();
         
         return log;
    }
}
