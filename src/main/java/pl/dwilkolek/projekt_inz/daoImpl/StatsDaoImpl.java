/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.daoImpl;

import org.hibernate.classic.Session;
import pl.dwilkolek.projekt_inz.dao.StatsDao;
import pl.dwilkolek.projekt_inz.hibernate.HibernateUtility;
import pl.dwilkolek.projekt_inz.model.Stats;

/**
 *
 * @author Slishu
 */
public class StatsDaoImpl implements StatsDao {

    private Session session;

    public StatsDaoImpl() {
        this.session = HibernateUtility.getSessionFactory().openSession();
    }

    @Override
    public Stats logData(Stats stats) {
        session.beginTransaction();
        session.save(stats);
        session.getTransaction().commit();
        return stats;
    }

}
