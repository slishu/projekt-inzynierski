/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.daoImpl;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.dwilkolek.projekt_inz.dao.SessionDao;
import pl.dwilkolek.projekt_inz.model.Session;
import pl.dwilkolek.projekt_inz.hibernate.HibernateUtility;

/**
 *
 * @author Slishu
 */
public class SessionDaoImpl implements SessionDao {

    private org.hibernate.Session session;

    public SessionDaoImpl() {

        this.session = HibernateUtility.getSessionFactory().openSession();

    }

    @Override
    public Session add(Session s) {
        session.beginTransaction();        
        session.save(s);        
        session.getTransaction().commit();
     
        return s;
    }

    @Override
    public Session update(Session s) {
        session.beginTransaction();
        session.update(s);
        session.getTransaction().commit();
    
        return s;
    }

    @Override
    public Session findBySid(long sid) {
        session.beginTransaction();
        Session s = (Session) session.get(Session.class, sid);
        session.getTransaction().commit();

        return s;
    }

}
