/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.daoImpl;

import org.hibernate.Session;
import pl.dwilkolek.projekt_inz.dao.UserDao;
import pl.dwilkolek.projekt_inz.model.User;
import pl.dwilkolek.projekt_inz.hibernate.HibernateUtility;


/**
 *
 * @author Slishu
 */
public class UserDaoImpl implements UserDao {

        private Session session;
  
    public UserDaoImpl() {
      
        this.session=HibernateUtility.getSessionFactory().openSession();
                
    }
    
    
    @Override
    public User add(User user) {

        session.beginTransaction();
        session.save(user);
        Long Uid = user.getUid();
        session.getTransaction().commit();
        

        return user;
    }

    @Override
    public User findByUid(long uid) {
        session.beginTransaction();
        User user = (User) session.get(User.class, uid);
        session.getTransaction().commit();
        
        return user;
    }


}
