/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.model;


import java.util.Date;
import pl.dwilkolek.projekt_inz.daoImpl.StatsDaoImpl;

/**
 *
 * @author Slishu
 */
public class EventLogger {

    public static Stats logData(String opisZdarzenia, TypyZdarzenEnum typZdarzeniaEnum) {
        Stats stats = new Stats(null,opisZdarzenia, new Date(), typZdarzeniaEnum.getValue());
        
        StatsDaoImpl statsDaoImpl = new StatsDaoImpl();
        stats = statsDaoImpl.logData(stats);     
        return stats;
    }
}
