/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.dwilkolek.projekt_inz.model;

/**
 *
 * @author Slishu
 */
public enum TypyZdarzenEnum {
    createUser(1), 
    createSession(2), 
    createLog(3), 
    log(4),
    getIp(5),
    getJs(6);
    
    private TypyZdarzenEnum(int i){
        value = i;
    }
    public int value;
    
    public int getValue(){
        return value;
    }
}
