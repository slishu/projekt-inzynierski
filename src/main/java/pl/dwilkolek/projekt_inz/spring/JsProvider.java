/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.spring;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.portlet.ModelAndView;
import pl.dwilkolek.projekt_inz.model.EventLogger;
import pl.dwilkolek.projekt_inz.model.TypyZdarzenEnum;
import pl.dwilkolek.projekt_inz.utility.Config;

@Controller
public class JsProvider {

    @RequestMapping(value = "/js", method = RequestMethod.GET)
    @ResponseBody()
    public String shareJs(HttpServletRequest request, HttpServletResponse response) {
        response.addHeader("Accept", "*/*");
        String jsContent = "";
        String nextPart;
        URL url = getClass().getResource("js.js");
        try {
            Scanner s = new Scanner(url.openStream());
            s.useDelimiter("\n\r");
            while (s.hasNext()) {
                nextPart = s.next();
                if (nextPart.contains("{#czasSesji#}")){
                    nextPart=nextPart.replace("{#czasSesji#}", Config.czasSesji.toString());
                }
                if (nextPart.contains("{#cid#}")){
                    nextPart=nextPart.replace("{#cid#}", request.getParameter("cid").toString());
                }
                jsContent += nextPart;
            }
        } catch (IOException ex) {
            Logger.getLogger(JsProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        EventLogger.logData("js shared", TypyZdarzenEnum.getJs);
        return jsContent;
    }


}
