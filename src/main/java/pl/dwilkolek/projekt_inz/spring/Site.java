/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.spring;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONStringer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.dwilkolek.projekt_inz.hibernate.HibernateUtility;
import pl.dwilkolek.projekt_inz.model.EventLogger;
import pl.dwilkolek.projekt_inz.model.TypyZdarzenEnum;
import pl.dwilkolek.projekt_inz.utility.Config;

@Controller()
public class Site {

    private org.hibernate.Session session = HibernateUtility.getSessionFactory().openSession();

    @RequestMapping(value = "/stats")
    public ModelAndView dbStats(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        Long dataSessionCount = ((Long) session.createQuery("select count(*) from Session").uniqueResult()).longValue();
        Long dataUserCount = ((Long) session.createQuery("select count(*) from User").uniqueResult()).longValue();
        Long dataLogCount = ((Long) session.createQuery("select count(*) from Log").uniqueResult()).longValue();
        Long dataStatsCount = ((Long) session.createQuery("select count(*) from Stats").uniqueResult()).longValue();

        Date date = new Date();

        Timestamp timestamp = new Timestamp(date.getTime() - Config.czasSesji * 60 * 1000);
        Long dataSessionActiveCount = ((Long) session.createQuery("select count(*) from Session where time_last > :czas")
                .setParameter("czas", timestamp)
                .uniqueResult()).longValue();

        Timestamp timestamp2 = new Timestamp(date.getTime() - 60 * 1000);
        Long msgRate = ((Long) session.createQuery("select count(*) from Stats where czas > :czas")
                .setParameter("czas", timestamp2)
                .uniqueResult()).longValue();

        Timestamp timestamp3 = new Timestamp(date.getTime() - 60000);
        Long activeUsers = ((Long) session.createQuery("select count(*) from Log where hrefLast > :czas")
                .setParameter("czas", timestamp3)
                .uniqueResult()).longValue();

        mav.addObject("dataSessionActiveCount", dataSessionActiveCount.toString());
        mav.addObject("dataLogCount", dataLogCount.toString());
        mav.addObject("dataSessionCount", dataSessionCount.toString());
        mav.addObject("dataUserCount", dataUserCount.toString());
        mav.addObject("dataStatsCount", dataStatsCount.toString());
        mav.addObject("msgRate", msgRate.toString());
        mav.addObject("dataActiveUsers", activeUsers.toString());
        return mav;
    }

    @RequestMapping(value = "/fullDojoStats")
    public ModelAndView fullDojoStats(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        return mav;
    }

    @RequestMapping(value = "/activeSessions")
    @ResponseBody()
    public String activeSessions(HttpServletRequest request, HttpServletResponse response) {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime() - Config.czasSesji * 60 * 1000);
        Long activeSessionsReturn = ((Long) session.createQuery("select count(*) from Session where time_last > :czas")
                .setParameter("czas", timestamp)
                .uniqueResult()).longValue();

        return activeSessionsReturn.toString();
    }

    @RequestMapping(value = "/userCount")
    @ResponseBody()
    public String userCount(HttpServletRequest request, HttpServletResponse response) {
        Long userCountReturn = ((Long) session.createQuery("select count(*) from User").uniqueResult()).longValue();
        return userCountReturn.toString();
    }

    @RequestMapping(value = "/logCount")
    @ResponseBody()
    public String logCount(HttpServletRequest request, HttpServletResponse response) {
        Long logCountReturn = ((Long) session.createQuery("select count(*) from Log").uniqueResult()).longValue();
        return logCountReturn.toString();
    }

    @RequestMapping(value = "/statsCount")
    @ResponseBody()
    public String statsCount(HttpServletRequest request, HttpServletResponse response) {
        Long statsCountReturn = ((Long) session.createQuery("select count(*) from Stats").uniqueResult()).longValue();
        return statsCountReturn.toString();
    }

    @RequestMapping(value = "/sesionCount")
    @ResponseBody()
    public String sessionCount(HttpServletRequest request, HttpServletResponse response) {
        Long sesionCountReturn = ((Long) session.createQuery("select count(*) from Session").uniqueResult()).longValue();
        return sesionCountReturn.toString();
    }

    @RequestMapping(value = "/msgRate")
    @ResponseBody()
    public String msgRate(HttpServletRequest request, HttpServletResponse response) {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime() - 60 * 1000);
        Long msgRateReturn = ((Long) session.createQuery("select count(*) from Stats where czas > :czas")
                .setParameter("czas", timestamp)
                .uniqueResult()).longValue();
        return msgRateReturn.toString();
    }

    @RequestMapping(value = "/avgTimeOnSite")
    @ResponseBody()
    public String avgTimeOnSite(HttpServletRequest request, HttpServletResponse response) {
        String avgTimeOnSiteReturn = "";
        List list = session.createQuery("select avg(time_last-time_start) || ' ' from Session").list();
        for (Iterator it = list.iterator(); it.hasNext();) {
            String object = (String) it.next();
            avgTimeOnSiteReturn = object;

        }
        return avgTimeOnSiteReturn;
    }

    @RequestMapping(value = "/avgSitesViewed")
    @ResponseBody()
    public String avgSitesViewed(HttpServletRequest request, HttpServletResponse response) {
        String avgSitesViewedReturn = "";
        List list = session.createSQLQuery("select avg(l) || ' ' from( select count(log.lid) as l from session join log on log.sid=session.sid group by session.sid) as tab").list();
        for (Iterator it = list.iterator(); it.hasNext();) {
            String object = (String) it.next();
            avgSitesViewedReturn = object;

        }
        return avgSitesViewedReturn;
    }

    @RequestMapping(value = "/ip")
    @ResponseBody()
    public String checkIp(HttpServletRequest request, HttpServletResponse response) {

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        EventLogger.logData("ip: " + ipAddress, TypyZdarzenEnum.getIp);
        return ipAddress;

    }

    Integer idActiveUsers = 0;

    @RequestMapping(value = "/activeUsers")
    @ResponseBody()
    public String activeUsers(HttpServletRequest request, HttpServletResponse response) {

        Date date = new Date();

        StringBuilder sb = new StringBuilder();
        sb.append("{\"items\":[");

        Timestamp timestamp3 = new Timestamp(date.getTime() - 25000);
        Long activeUsersReturn = ((Long) session.createQuery("select count(*) from Log where hrefLast > :czas")
                .setParameter("czas", timestamp3)
                .uniqueResult()).longValue();

        sb.append("{ \"id\": " + idActiveUsers + ", \"value\": " + activeUsersReturn + " }");
        idActiveUsers++;
        sb.append("]}");

        return sb.toString();

    }

    @RequestMapping(value = "/bestBrowser")
    @ResponseBody()
    public String bestBrowser(HttpServletRequest request, HttpServletResponse response) {
        Long ff = ((Long) session.createQuery("select count(*) from Session WHERE browser LIKE '%Firefox%'").uniqueResult()).longValue();
        Long opera = ((Long) session.createQuery("select count(*) from Session WHERE browser LIKE '%Opera%'").uniqueResult()).longValue();
        Long safari = ((Long) session.createQuery("select count(*)  from Session WHERE  browser LIKE '%Safari%' AND browser NOT LIKE '%Chrome%'").uniqueResult()).longValue();
        Long chrome = ((Long) session.createQuery("select count(*)  from Session WHERE browser LIKE '%Chrome%'").uniqueResult()).longValue();
        Long ie = ((Long) session.createQuery("select count(*)  from Session WHERE browser LIKE '%MSIE%' or browser LIKE '%Trident%'").uniqueResult()).longValue();
        StringBuilder sb = new StringBuilder();
        Long suma = ff + chrome + safari + opera;
        Double ffval = Math.round(ff.doubleValue() / suma.doubleValue() * 10000.0) / 100.00;
        Double opval = Math.round(opera.doubleValue() / suma.doubleValue() * 10000.0) / 100.00;
        Double saval = Math.round(safari.doubleValue() / suma.doubleValue() * 10000.0) / 100.00;
        Double chval = Math.round(chrome.doubleValue() / suma.doubleValue() * 10000.0) / 100.00;
        Double ieval = Math.round(ie.doubleValue() / suma.doubleValue() * 10000.0) / 100.00;
        sb.append("[");
        sb.append("{ \"y\": " + ff + ", \"text\": " + "\"Firefox\"" + " , \"tooltip\" : \"" + ffval + "% - " + ff + "sesji\" },");
        sb.append("{  \"y\": " + opera + ", \"text\": " + "\"Opera\"" + " , \"tooltip\" : \"" + opval + "% - " + opera + "sesji\" },");
        sb.append("{  \"y\": " + safari + ", \"text\": " + "\"Chrome\"" + " , \"tooltip\" : \"" + saval + "% - " + safari + "sesji\"}, ");
        sb.append("{  \"y\": " + chrome + ", \"text\": " + "\"Safari\"" + " , \"tooltip\" : \"" + chval + "% - " + chrome + "sesji\"}, ");
        sb.append("{  \"y\": " + ie + ", \"text\": " + "\"Internet Explorer\"" + " , \"tooltip\" : \"" + ieval + "% - " + ie + "sesji\"} ");
        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/bestOs")
    @ResponseBody()
    public String bestOs(HttpServletRequest request, HttpServletResponse response) {

        StringBuilder sb = new StringBuilder();
        List list = session.createSQLQuery("Select count(*), os from Session Where os <> '' and os <> 'Unknown OS' group by os").list();
        Integer suma = 0;
        sb.append("[");
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] object = (Object[]) it.next();
            Integer intE = new Integer(object[0].toString());
            suma = suma + intE;
        }
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;

            Object[] object = (Object[]) it.next();
            Double val = new Double(object[0].toString());
            System.out.println("suma:" + (val / suma / 1.0) + " " + val + " " + suma);
            Double valPro = Math.round(val / suma * 10000) / 100.00;
            sb.append("{  \"y\": " + object[0].toString() + ", \"text\": " + "\"" + object[1].toString() + "\"" + " , \"tooltip\" : \"" + valPro.toString() + "% - " + object[0].toString() + "sesji\"} ");

        }
        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/bestSite")
    @ResponseBody()
    public String bestSite(HttpServletRequest request, HttpServletResponse response) {

        StringBuilder sb = new StringBuilder();
        List list = session.createSQLQuery("select COUNT(*) as l,HREF from log group by href order by l desc LIMIT 500").list();
        Integer suma = 0;
        sb.append("[");
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] object = (Object[]) it.next();
            Integer intE = new Integer(object[0].toString());
            suma = suma + intE;
        }
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;

            Object[] object = (Object[]) it.next();
            String addr = object[1].toString();
            addr = addr.replace("http://www.dzielnica24.pl", "");
            System.out.println("adres: "+addr);
            Double val = new Double(object[0].toString());
            Double valPro = Math.round(val / suma * 10000) / 100.00;
            sb.append("{  \"lp\": " + i + ", \"adres\": " + "\"" + addr +"\"" + " , \"wejsc\" : " + val + ", \"wejscpro\" : \"" + valPro + "\"} ");

        }
        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/bestResolution")
    @ResponseBody()
    public String bestResolution(HttpServletRequest request, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();
        List list = session.createSQLQuery("(Select count(*), resolution from Session Where resolution <> '' group by resolution order by count(*) desc limit 8)").list();
        Integer suma = 0;
        sb.append("[");
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] object = (Object[]) it.next();
            Integer intE = new Integer(object[0].toString());
            suma = suma + intE;
        }
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;
            Object[] object = (Object[]) it.next();
            Double val = new Double(object[0].toString());
            System.out.println("suma:" + (val / suma / 1.0) + " " + val + " " + suma);
            Double valPro = Math.round(val / suma * 10000) / 100.00;

            sb.append("{  \"y\": " + object[0].toString() + ", \"text\": " + "\"" + object[1].toString() + "\"" + " , \"tooltip\" : \"" + valPro + "% - " + object[0].toString() + "sesji\"} ");

        }
        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/uniqueUsersPerDay")
    @ResponseBody()
    public String uniqueUsersPerDay(HttpServletRequest request, HttpServletResponse response) {
        List list = session.createSQLQuery("SELECT day,count(uid) FROM ( SELECT distinct uid,date_trunc('day', time_start) AS day from Session ) as tab group by day order by day limit 30").list();

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;

            Object[] object = (Object[]) it.next();

            sb.append("{ \"x\": \"" + i + "\", \"y\": " + object[1].toString() + ", \"text\": \"" + object[0].toString().replace(" 00:00:00.0", "") + "\" , \"tooltip\" : \"" + object[1].toString() + "\" } ");

        }

        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/usersPerDay")
    @ResponseBody()
    public String usersPerDay(HttpServletRequest request, HttpServletResponse response) {
        List list = session.createSQLQuery("SELECT date_trunc('day', time_start) AS day, count(sid) AS user_count FROM \"user\" JOIN session on \"user\".uid = Session.uid GROUP BY date_trunc('day', time_start) Order BY day Limit 30").list();

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;

            Object[] object = (Object[]) it.next();
            sb.append("{ \"x\":\"" + i + "\", \"y\": " + object[1].toString() + ", \"text\": \"" + object[0].toString().replace(" 00:00:00.0", "") + "\" , \"tooltip\" : \"" + object[1].toString() + "\" } ");

        }

        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/logsPerDay")
    @ResponseBody()
    public String logsPerDay(HttpServletRequest request, HttpServletResponse response) {
        List list = session.createSQLQuery("select date_trunc('day', href_start) AS day,count(*) from log group by date_trunc('day', href_start) order by day LIMIT 30").list();

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;

            Object[] object = (Object[]) it.next();
            sb.append("{ \"x\":\"" + i + "\", \"y\": " + object[1].toString() + ", \"text\": \"" + object[0].toString().replace(" 00:00:00.0", "") + "\" , \"tooltip\" : \"" + object[1].toString() + "\" } ");

        }

        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/refuseProportion")
    @ResponseBody()
    public String refuseProportion(HttpServletRequest request, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();
        List list = session.createSQLQuery("SELECT sum(l),'Normalne wejścia' as co FROM ( Select session.sid,count(log.lid) as l from session join log on log.sid=session.sid group by session.sid having count(log.lid)<>1) as tab union Select sum(l),'Porzucenia' as co FROM ( Select session.sid,count(log.lid) as l from session join log on log.sid=session.sid group by session.sid having count(log.lid)=1) as tab").list();
        Integer suma = 0;
        sb.append("[");
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] object = (Object[]) it.next();
            Integer intE = new Integer(object[0].toString());
            suma = suma + intE;
        }
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;
            Object[] object = (Object[]) it.next();
            Double val = new Double(object[0].toString());
            System.out.println("suma:" + (val / suma / 1.0) + " " + val + " " + suma);
            Double valPro = Math.round(val / suma * 10000) / 100.00;

            sb.append("{  \"y\": " + object[0].toString() + ", \"text\": " + "\"" + object[1].toString() + "\"" + " , \"tooltip\" : \"" + valPro + "% - " + object[0].toString() + "sesji\"} ");

        }
        sb.append("]");

        return sb.toString();

    }

    @RequestMapping(value = "/returningVisitors")
    @ResponseBody()
    public String returningVisitors(HttpServletRequest request, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();
        List list = session.createSQLQuery("select 	COUNT(*), 'Nowi użytkownicy' as co from ( SELECT session.uid,count(session.sid) from session join \"user\" on session.uid=\"user\".uid group by session.uid having count(session.sid)=1) as tab union select 	COUNT(*), 'Powracający użytkownicy' as co from ( SELECT session.uid,count(session.sid) from session join \"user\" on session.uid=\"user\".uid group by session.uid having count(session.sid)<>1) as tab").list();
        Integer suma = 0;
        sb.append("[");
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] object = (Object[]) it.next();
            Integer intE = new Integer(object[0].toString());
            suma = suma + intE;
        }
        Integer i = 0;
        for (Iterator it = list.iterator(); it.hasNext();) {
            if (i != 0) {
                sb.append(",");
            }
            i++;
            Object[] object = (Object[]) it.next();
            Double val = new Double(object[0].toString());
            System.out.println("suma:" + (val / suma / 1.0) + " " + val + " " + suma);
            Double valPro = Math.round(val / suma * 10000) / 100.00;

            sb.append("{  \"y\": " + object[0].toString() + ", \"text\": " + "\"" + object[1].toString() + "\"" + " , \"tooltip\" : \"" + valPro + "% - " + object[0].toString() + "sesji\"} ");

        }
        sb.append("]");

        return sb.toString();

    }
}
