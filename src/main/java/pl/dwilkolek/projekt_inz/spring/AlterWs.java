/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.spring;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.dwilkolek.projekt_inz.daoImpl.LogDaoImpl;
import pl.dwilkolek.projekt_inz.daoImpl.SessionDaoImpl;
import pl.dwilkolek.projekt_inz.daoImpl.UserDaoImpl;
import pl.dwilkolek.projekt_inz.model.EventLogger;
import pl.dwilkolek.projekt_inz.model.Log;
import pl.dwilkolek.projekt_inz.model.Session;
import pl.dwilkolek.projekt_inz.model.TypyZdarzenEnum;
import pl.dwilkolek.projekt_inz.model.User;
import pl.dwilkolek.projekt_inz.utility.Config;

/**
 *
 * @author Slishu
 */
@Controller
public class AlterWs {

    @RequestMapping(value = "/logByJs", method = RequestMethod.GET)
    @ResponseBody()
    public String logByGet(HttpServletRequest request, HttpServletResponse response) {
        String cid = request.getParameter("cid").toString();
        String uid = request.getParameter("uid").toString();
        String sid = request.getParameter("sid").toString();
        String lid = "";
        String browser = request.getParameter("browser").toString();
        String os = request.getParameter("os").toString();
        String resolution = request.getParameter("resolution").toString();
        String href = request.getParameter("href").toString();
        String refferal = request.getParameter("refferal").toString();

        if (uid.isEmpty()) {
            User user;
            UserDaoImpl userDaoImpl = new UserDaoImpl();

            user = userDaoImpl.add(new User(null, new Date()));
            EventLogger.logData("createUser: " + user.getUid(), TypyZdarzenEnum.createUser);
            uid = user.getUid().toString();
        }
        if (sid.isEmpty()) {
            SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
            UserDaoImpl userDaoImpl = new UserDaoImpl();
            Session session;
            User user;

            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }

            user = userDaoImpl.findByUid(new Long(uid));

            session = sessionDaoImpl.add(
                    new Session(null, user, new Date(), new Date(), ipAddress, browser, new Integer(cid), os, resolution, null)
            );

            EventLogger.logData("createSession: " + session.getSid().toString(), TypyZdarzenEnum.createSession);

            sid = session.getSid().toString();
        }

        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
        LogDaoImpl logDaoImpl = new LogDaoImpl();
        Log log = new Log(null, sessionDaoImpl.findBySid(new Long(sid)), href, new Date(), new Date(), refferal);
        log = logDaoImpl.add(log);
        EventLogger.logData("createLog: " + log.getLid(), TypyZdarzenEnum.createLog);
        lid = log.getLid().toString();

        return "window.turbit_uid=" + uid + ";" + "window.turbit_sid=" + sid + ";" + "window.turbit_lid=" + lid + ";window.connectorTurbitInstance.saveLogByJs();";

    }

    @RequestMapping(value = "/logLid", method = RequestMethod.GET)
    @ResponseBody()
    public String logLid(HttpServletRequest request, HttpServletResponse response) {
        
        
        String lid = request.getParameter("lid").toString();
        
        LogDaoImpl logDaoImpl = new LogDaoImpl();
        Log log = logDaoImpl.findByLid(new Long(lid));
        Long currentLong = new Date().getTime();
        Long itsLong = log.getHrefLast().getTime();

        if ((currentLong - itsLong) < Config.czasNieaktywnosciKarty * 1000) {
            log.setHrefLast(new Timestamp(new Date().getTime()));
            log = logDaoImpl.update(log);
        } else {
            Log nLog = new Log(null, log.getSession(), log.getHref(), new Date(), new Date(), log.getHref());
            log = logDaoImpl.add(nLog);
        }

        Long sid = log.getSession().getSid();

        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
        Session s = sessionDaoImpl.findBySid(sid);
        s.setTimeLast(new Timestamp(new Date().getTime()));
        sessionDaoImpl.update(s);

        EventLogger.logData("log: " + lid, TypyZdarzenEnum.log);

        return "window.connectorTurbitInstance.lid="+log.getLid().toString()+";";

    }
}
