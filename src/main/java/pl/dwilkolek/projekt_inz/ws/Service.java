/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dwilkolek.projekt_inz.ws;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import static javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING;
import pl.dwilkolek.projekt_inz.daoImpl.LogDaoImpl;

import pl.dwilkolek.projekt_inz.daoImpl.SessionDaoImpl;
import pl.dwilkolek.projekt_inz.daoImpl.UserDaoImpl;
import pl.dwilkolek.projekt_inz.hibernate.HibernateUtility;
import pl.dwilkolek.projekt_inz.model.EventLogger;
import pl.dwilkolek.projekt_inz.model.Log;
import pl.dwilkolek.projekt_inz.model.Session;

import pl.dwilkolek.projekt_inz.model.TypyZdarzenEnum;
import pl.dwilkolek.projekt_inz.model.User;
import pl.dwilkolek.projekt_inz.utility.Config;

/**
 *
 * @author Slishu
 */
@WebService(serviceName = "Service")
@BindingType(value = SOAP12HTTP_BINDING)
public class Service {

    @Resource
    WebServiceContext wsContext;

    private org.hibernate.classic.Session session;

    public Service() {
        session = HibernateUtility.getSessionFactory().openSession();
    }

    @WebMethod(operationName = "fLog")
    public String fLog(
            @WebParam(name = "uid") String uid, @WebParam(name = "browser") String browser, @WebParam(name = "cid") String cid, @WebParam(name = "os") String os, @WebParam(name = "resolution") String resolution,
            @WebParam(name = "sid") String sid, @WebParam(name = "href") String href, @WebParam(name = "refferal") String refferal,
            @WebParam(name = "lid") String lid
    ) {
        if (uid.isEmpty()) {
            User user;
            UserDaoImpl userDaoImpl = new UserDaoImpl();

            user = userDaoImpl.add(new User(null, new Date()));
            EventLogger.logData("createUser: " + user.getUid(), TypyZdarzenEnum.createUser);
            uid = user.getUid().toString();
        }
        if (sid.isEmpty()) {
            SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
            UserDaoImpl userDaoImpl = new UserDaoImpl();
            Session session;
            User user;

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest request = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }

            user = userDaoImpl.findByUid(new Long(uid));

            session = sessionDaoImpl.add(
                    new Session(null, user, new Date(), new Date(), ipAddress, browser, new Integer(cid), os, resolution, null)
            );

            EventLogger.logData("createSession: " + session.getSid().toString(), TypyZdarzenEnum.createSession);

            sid = session.getSid().toString();
        }

        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
        LogDaoImpl logDaoImpl = new LogDaoImpl();
        Log log = new Log(null, sessionDaoImpl.findBySid(new Long(sid)), href, new Date(), new Date(), refferal);
        log = logDaoImpl.add(log);
        EventLogger.logData("createLog: " + log.getLid(), TypyZdarzenEnum.createLog);
        lid = log.getLid().toString();

        return uid + "#" + sid + "#" + lid;
    }

    @WebMethod(operationName = "log")
    public String log(@WebParam(name = "lid") String lid) {
        LogDaoImpl logDaoImpl = new LogDaoImpl();
        Log log = logDaoImpl.findByLid(new Long(lid));
        Long currentLong = new Date().getTime();
        Long itsLong = log.getHrefLast().getTime();

        if ((currentLong - itsLong) < Config.czasNieaktywnosciKarty * 1000) {
            log.setHrefLast(new Timestamp(new Date().getTime()));
            log = logDaoImpl.update(log);
        } else {
            Log nLog = new Log(null, log.getSession(), log.getHref(), new Date(), new Date(), log.getHref());
            log = logDaoImpl.add(nLog);
        }

        Long sid = log.getSession().getSid();

        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
        Session s = sessionDaoImpl.findBySid(sid);
        s.setTimeLast(new Timestamp(new Date().getTime()));
        sessionDaoImpl.update(s);

        EventLogger.logData("log: " + lid, TypyZdarzenEnum.log);

        return log.getLid().toString();
    }

    @WebMethod(operationName = "createLog")
    public String createLog(@WebParam(name = "sid") String sid, @WebParam(name = "href") String href, @WebParam(name = "refferal") String refferal) {
        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
        LogDaoImpl logDaoImpl = new LogDaoImpl();
        Log log = new Log(null, sessionDaoImpl.findBySid(new Long(sid)), href, new Date(), new Date(), refferal);
        log = logDaoImpl.add(log);

        EventLogger.logData("createLog: " + log.getLid(), TypyZdarzenEnum.createLog);

        return log.getLid().toString();
    }

    @WebMethod(operationName = "createSession")
    public String createSession(@WebParam(name = "uid") String uid, @WebParam(name = "browser") String browser, @WebParam(name = "cid") String cid, @WebParam(name = "os") String os, @WebParam(name = "resolution") String resolution) throws IOException {
        SessionDaoImpl sessionDaoImpl = new SessionDaoImpl();
        UserDaoImpl userDaoImpl = new UserDaoImpl();
        Session session;
        User user;

        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest request = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        if (uid.isEmpty()) {
            uid = createUser();
        }
        user = userDaoImpl.findByUid(new Long(uid));

        session = sessionDaoImpl.add(
                new Session(null, user, new Date(), new Date(), ipAddress, browser, new Integer(cid), os, resolution, null)
        );

        EventLogger.logData("createSession: " + session.getSid().toString(), TypyZdarzenEnum.createSession);

        return session.getSid().toString();
    }

    @WebMethod(operationName = "createUser")
    public String createUser() throws FileNotFoundException, IOException {
        String errors;
        User user;
        UserDaoImpl userDaoImpl = new UserDaoImpl();

        user = userDaoImpl.add(new User(null, new Date()));
        EventLogger.logData("createUser: " + user.getUid(), TypyZdarzenEnum.createUser);
        return user.getUid().toString();

    }
}
